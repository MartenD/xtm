﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DeviceLibrary.Devices.Deposition;
using System.Threading;
using Microsoft.Win32;
using OxyPlot;
using OxyPlot.Series;

namespace XTMMonitor
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private XTM2 xtc;
        public XTC2.Configparameter config
        {
            get {
                return xtc.GetConfig();
            }
        }
        public bool Capture
        {
            get;
            private set;
        }
        private struct LoggedDataPoint
        {
            public TimeSpan Time { get; set; }
            public float Rate { get; set; }
            public float Thickness { get; set; }
        }

        public delegate void UpdateCallback();
        private bool WorkerRunning { set; get; }

        private Thread update;

        private ManualResetEvent _endCaptureEvent = new ManualResetEvent(false);
        private List<LoggedDataPoint> loggedData = new List<LoggedDataPoint>();
        public DateTime CaptureStart { private set; get; } = DateTime.Now;
        public MainWindow(string portAdr)
        {
            InitializeComponent();
            try
            {
                xtc = new XTM2();
                var port = new System.IO.Ports.SerialPort(portAdr);
                port.BaudRate = 9600;
                xtc.Connect(port);

                update = new Thread(UpdateThread);
                update.IsBackground = true;
                update.Start();

            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to connect to XTM on adresse "+ portAdr+".\nMsg: " + e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            }
            Capture = false;
            RatePlot.Series[0].ItemsSource = new List<DataPoint>();
            RatePlot.Series[1].ItemsSource = new List<DataPoint>();
        }
          private void UpdateThread()
        {
            WorkerRunning = true;
            while (WorkerRunning)
            {
                Dispatcher.Invoke(
                    new UpdateCallback(this.Update)
                );
                Thread.Sleep(500);
            }
        }
        private void Update()
        {
            //_context.UpdateStatus();
            var processInfo = xtc.ProcessInfo;
            RateDisplay.Text = processInfo.Rate.ToString("N2");
            ThicknessDisplay.Text = processInfo.Thickness.ToString("N2");


            if (Capture)
            {
                var deltaTime = DateTime.Now.Subtract(CaptureStart);
                TimerDisplay.Text = deltaTime.ToString(@"mm\:ss");
                (RatePlot.Series[0].ItemsSource as List<DataPoint>).Add(new DataPoint(deltaTime.TotalSeconds, processInfo.Rate));
                (RatePlot.Series[1].ItemsSource as List<DataPoint>).Add(new DataPoint(deltaTime.TotalSeconds, processInfo.Thickness));
                RatePlot.InvalidatePlot(true);

                var dataPoint = new LoggedDataPoint { Time = deltaTime, Rate = processInfo.Rate, Thickness = processInfo.Thickness};
                loggedData.Add(dataPoint);
                CaptureLED.Fill = new SolidColorBrush(Colors.Green);
            }
            else
            {
                CaptureLED.Fill = new SolidColorBrush(Colors.LightGray);
                TimerDisplay.Text = "00:00";
            }

        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            xtc.Start();
            CaptureStart = DateTime.Now;
            (RatePlot.Series[0].ItemsSource as List<DataPoint>).Clear();
            (RatePlot.Series[1].ItemsSource as List<DataPoint>).Clear();
            loggedData.Clear();
            xtc.Start();
            Capture = true;
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            Capture = false;
            xtc.Stop();
            if (MessageBox.Show("Daten speichern?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                SaveFileDialog cdialog = new SaveFileDialog()
                {
                    Filter = "TXT Files(*.txt)|*.txt|All(*.*)|*"
                };

                if (cdialog.ShowDialog() == true)
                {
                    SaveLoggedProcessData(cdialog.FileName);
                }
                else
                {
                    MessageBox.Show("Saving aborted.", "Abort", MessageBoxButton.OK);
                }
            }
            else
            {

            }
        }

        private void ZeroButton_Click(object sender, RoutedEventArgs e)
        {
            xtc.Zero();
        }

        public void SaveLoggedProcessData(string name)
        {
            using (System.IO.StreamWriter file =
          new System.IO.StreamWriter(name))
            {
                file.WriteLine("Time[seconds]\tRate[A/s]\tThickness[A]");
                foreach (LoggedDataPoint line in loggedData)
                {

                    file.WriteLine(line.Time.TotalSeconds.ToString("N1") + '\t' + line.Rate.ToString("N2") + '\t' + line.Thickness.ToString("N2"));

                }

            }
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("XTM Monitor by Dennis Meyer\nVersion: 21.06.2018\ndennis.meyer@phys.uni-goettingen.de", "About", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
